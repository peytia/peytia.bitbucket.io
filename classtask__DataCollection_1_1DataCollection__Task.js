var classtask__DataCollection_1_1DataCollection__Task =
[
    [ "__init__", "classtask__DataCollection_1_1DataCollection__Task.html#a3a735ea321bcfe21cf7cb7394d732769", null ],
    [ "updateDataCollection", "classtask__DataCollection_1_1DataCollection__Task.html#a7dd797727a2f2f4942984b4873b6c391", null ],
    [ "action", "classtask__DataCollection_1_1DataCollection__Task.html#a851a8c9097351a03adaa918d0abd0a3d", null ],
    [ "angle", "classtask__DataCollection_1_1DataCollection__Task.html#a2d0ec2285c8995d345ae727be078f78f", null ],
    [ "collectedThetaX", "classtask__DataCollection_1_1DataCollection__Task.html#a29b95bc248641b18fb67a5c7395c3842", null ],
    [ "collectedThetaY", "classtask__DataCollection_1_1DataCollection__Task.html#ac15635d04c8b11ccc689105d3d8e58c6", null ],
    [ "collectedTimeData", "classtask__DataCollection_1_1DataCollection__Task.html#ab7daeb3b984d43881529bfca4aad88aa", null ],
    [ "collectedXData", "classtask__DataCollection_1_1DataCollection__Task.html#ab79a130cc7ef762550ef6849e221bb0c", null ],
    [ "collectedYData", "classtask__DataCollection_1_1DataCollection__Task.html#a3fa2dc0f84f2de023702903f965911af", null ],
    [ "collectionState", "classtask__DataCollection_1_1DataCollection__Task.html#aa5d305e2a0a863025ebce6ce8654f5cf", null ],
    [ "collectionTimer", "classtask__DataCollection_1_1DataCollection__Task.html#a74854fcf9615b1f62ed9be4981e00de6", null ],
    [ "collectionTimerStart", "classtask__DataCollection_1_1DataCollection__Task.html#a3fed5627ced2d9f6637de570f43669f1", null ],
    [ "eulerTuple", "classtask__DataCollection_1_1DataCollection__Task.html#a1ab99f125ffdb3a8e03fa78e127dd009", null ],
    [ "position", "classtask__DataCollection_1_1DataCollection__Task.html#a6c34b352dd69787374d6c8fd8a906561", null ],
    [ "positionTuple", "classtask__DataCollection_1_1DataCollection__Task.html#afb847f931641cef1b8f8f255543c56b7", null ],
    [ "TimFlipFlop", "classtask__DataCollection_1_1DataCollection__Task.html#a98e9a344bdc553ddfd931b000429b915", null ]
];