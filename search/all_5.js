var searchData=
[
  ['datacollection_0',['dataCollection',['../main__LabFF_8py.html#a433da07481dab06953bc35f9c9af4f1d',1,'main_LabFF']]],
  ['datacollection_5ftask_1',['DataCollection_Task',['../classtask__DataCollection_1_1DataCollection__Task.html',1,'task_DataCollection']]],
  ['delta_2',['delta',['../classencoder_1_1Encoder.html#ad017c0a5f382fe0dac6ed8920ce90635',1,'encoder::Encoder']]],
  ['deltaposition_3',['deltaPosition',['../classtask__encoder_1_1Encoder__task.html#a6973ee764a836e9593a6f237113ed68b',1,'task_encoder::Encoder_task']]],
  ['deltaposition_5fshare_4',['deltaPosition_Share',['../main__Lab4_8py.html#ae68f7c39584d6511c3e69502768bb157',1,'main_Lab4']]],
  ['deltat_5',['deltaT',['../classClosedLoop_1_1ClosedLoop.html#a5326172a458db1ecfcce13677c9d072c',1,'ClosedLoop.ClosedLoop.deltaT()'],['../classtask__Controller_1_1Controller__Task.html#a4925b6e2a0a80491fbed0acbd9cebbba',1,'task_Controller.Controller_Task.deltaT()']]],
  ['desiredomega_6',['desiredOmega',['../classClosedLoop_1_1ClosedLoop.html#a86dddb52df2621d4c458a76d447dcd8e',1,'ClosedLoop::ClosedLoop']]],
  ['disable_7',['disable',['../classDRV8847_1_1DRV8847.html#acd9dbef9212b3014eab18a57a6e0f13a',1,'DRV8847::DRV8847']]],
  ['drv8847_8',['DRV8847',['../classDRV8847_1_1DRV8847.html',1,'DRV8847']]],
  ['drv8847_2epy_9',['DRV8847.py',['../DRV8847_8py.html',1,'']]],
  ['duration_10',['duration',['../main__Lab1_8py.html#afc858e3b7ad5582623d916ad7f4e6cd9',1,'main_Lab1']]],
  ['duty_5fshare_11',['duty_Share',['../main__Lab3_8py.html#aeeac2b17f6a0daa74eaab5bd2409b175',1,'main_Lab3.duty_Share()'],['../main__Lab4_8py.html#a1e98bb16f3308d66fd10f84ac0460d61',1,'main_Lab4.duty_Share()']]]
];
