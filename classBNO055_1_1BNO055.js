var classBNO055_1_1BNO055 =
[
    [ "__init__", "classBNO055_1_1BNO055.html#accc9922ea3ae6b8cec17e66d7c304aad", null ],
    [ "Acceleration", "classBNO055_1_1BNO055.html#aebbcde346b01c244beb56b68a70bc96d", null ],
    [ "calibConstant", "classBNO055_1_1BNO055.html#ae68147d22785123bd821c4b3442ca435", null ],
    [ "calibStatus", "classBNO055_1_1BNO055.html#a8f1cf7e57d6524ecba0f2e2994137511", null ],
    [ "EulerAngle", "classBNO055_1_1BNO055.html#a5a58abdf6a31c8d38768baf9f68e269c", null ],
    [ "oper_mode", "classBNO055_1_1BNO055.html#acfd3905925040f2729826f61ba05c8d2", null ],
    [ "acc", "classBNO055_1_1BNO055.html#afc13ab4fc65a9cec57509b3ce54357a6", null ],
    [ "acc_hexval", "classBNO055_1_1BNO055.html#a02389028165df321253cd971fd65f80a", null ],
    [ "acc_unscaled", "classBNO055_1_1BNO055.html#a7dce7fd220e374ede204c8ec62b34b5e", null ],
    [ "bufcal", "classBNO055_1_1BNO055.html#a8fa7cd3dca896367ce498d7ddbb4863f", null ],
    [ "buffer_accel", "classBNO055_1_1BNO055.html#ad6add589eb6a8c677201a33e7c399178", null ],
    [ "buffer_euler", "classBNO055_1_1BNO055.html#a8935800be8c6613c3bb1e89f487e2b65", null ],
    [ "eulerAng", "classBNO055_1_1BNO055.html#afc3720c50c5cf16dce054b6926c3ccda", null ],
    [ "eulerAng_hexval", "classBNO055_1_1BNO055.html#ab734493809a8c06f17a5f4891b79c92e", null ],
    [ "eulerAng_unscaled", "classBNO055_1_1BNO055.html#a51466c07461af10273d71372343c5dc0", null ],
    [ "i2c", "classBNO055_1_1BNO055.html#a8eebc88bf7ecfe69323da2b089911508", null ]
];