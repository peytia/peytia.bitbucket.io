var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_Controller", null, [
      [ "Controller_Task", "classtask__Controller_1_1Controller__Task.html", "classtask__Controller_1_1Controller__Task" ]
    ] ],
    [ "task_DataCollection", null, [
      [ "DataCollection_Task", "classtask__DataCollection_1_1DataCollection__Task.html", "classtask__DataCollection_1_1DataCollection__Task" ]
    ] ],
    [ "task_encoder", null, [
      [ "Encoder_task", "classtask__encoder_1_1Encoder__task.html", "classtask__encoder_1_1Encoder__task" ]
    ] ],
    [ "task_IMU", null, [
      [ "IMU_Task", "classtask__IMU_1_1IMU__Task.html", "classtask__IMU_1_1IMU__Task" ]
    ] ],
    [ "task_Motor", null, [
      [ "Motor_task", "classtask__Motor_1_1Motor__task.html", "classtask__Motor_1_1Motor__task" ]
    ] ],
    [ "task_TouchPanel", null, [
      [ "Touch_Task", "classtask__TouchPanel_1_1Touch__Task.html", "classtask__TouchPanel_1_1Touch__Task" ]
    ] ],
    [ "task_User", null, [
      [ "User_task", "classtask__User_1_1User__task.html", "classtask__User_1_1User__task" ]
    ] ],
    [ "TouchPanel", null, [
      [ "TouchPanel", "classTouchPanel_1_1TouchPanel.html", "classTouchPanel_1_1TouchPanel" ]
    ] ]
];