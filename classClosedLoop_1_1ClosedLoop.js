var classClosedLoop_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop_1_1ClosedLoop.html#a2e4d1f7e3bef91aa026ffdf546150bac", null ],
    [ "update_closed_loop", "classClosedLoop_1_1ClosedLoop.html#aae269d04b78e8372aabcd67f7f8fe879", null ],
    [ "currentDelta", "classClosedLoop_1_1ClosedLoop.html#a22bf47f3ce3d33dae4ab870902f4dc38", null ],
    [ "currentKp", "classClosedLoop_1_1ClosedLoop.html#a9af3a62c6247cbc51fe8d3585bb5ae6e", null ],
    [ "currentOmega", "classClosedLoop_1_1ClosedLoop.html#a4b1be5e3c2fa4494b361ee8cd3fc2f33", null ],
    [ "currentTime", "classClosedLoop_1_1ClosedLoop.html#ab5f8a36dcca2945f244a943bc1bb6a6e", null ],
    [ "deltaT", "classClosedLoop_1_1ClosedLoop.html#a5326172a458db1ecfcce13677c9d072c", null ],
    [ "desiredOmega", "classClosedLoop_1_1ClosedLoop.html#a86dddb52df2621d4c458a76d447dcd8e", null ],
    [ "L_desired", "classClosedLoop_1_1ClosedLoop.html#a5aec8e720680c8a4d2d40ed00b05b050", null ],
    [ "previousTime", "classClosedLoop_1_1ClosedLoop.html#ae804ffe0a1566b02e8640a833beb04cf", null ],
    [ "time", "classClosedLoop_1_1ClosedLoop.html#abe681cb73fb23e4cfdf8c94da8eb4f9c", null ]
];