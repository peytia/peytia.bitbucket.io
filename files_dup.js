var files_dup =
[
    [ "ClosedLoop.py", "ClosedLoop_8py.html", "ClosedLoop_8py" ],
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main_Lab1.py", "main__Lab1_8py.html", "main__Lab1_8py" ],
    [ "main_Lab2.py", "main__Lab2_8py.html", null ],
    [ "main_Lab3.py", "main__Lab3_8py.html", "main__Lab3_8py" ],
    [ "main_Lab4.py", "main__Lab4_8py.html", "main__Lab4_8py" ],
    [ "main_LabFF.py", "main__LabFF_8py.html", "main__LabFF_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_Controller.py", "task__Controller_8py.html", [
      [ "task_Controller.Controller_Task", "classtask__Controller_1_1Controller__Task.html", "classtask__Controller_1_1Controller__Task" ]
    ] ],
    [ "task_DataCollection.py", "task__DataCollection_8py.html", [
      [ "task_DataCollection.DataCollection_Task", "classtask__DataCollection_1_1DataCollection__Task.html", "classtask__DataCollection_1_1DataCollection__Task" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.Encoder_task", "classtask__encoder_1_1Encoder__task.html", "classtask__encoder_1_1Encoder__task" ]
    ] ],
    [ "task_IMU.py", "task__IMU_8py.html", [
      [ "task_IMU.IMU_Task", "classtask__IMU_1_1IMU__Task.html", "classtask__IMU_1_1IMU__Task" ]
    ] ],
    [ "task_Motor.py", "task__Motor_8py.html", [
      [ "task_Motor.Motor_task", "classtask__Motor_1_1Motor__task.html", "classtask__Motor_1_1Motor__task" ]
    ] ],
    [ "task_TouchPanel.py", "task__TouchPanel_8py.html", [
      [ "task_TouchPanel.Touch_Task", "classtask__TouchPanel_1_1Touch__Task.html", "classtask__TouchPanel_1_1Touch__Task" ]
    ] ],
    [ "task_User.py", "task__User_8py.html", [
      [ "task_User.User_task", "classtask__User_1_1User__task.html", "classtask__User_1_1User__task" ]
    ] ],
    [ "TouchPanel.py", "TouchPanel_8py.html", [
      [ "TouchPanel.TouchPanel", "classTouchPanel_1_1TouchPanel.html", "classTouchPanel_1_1TouchPanel" ]
    ] ]
];